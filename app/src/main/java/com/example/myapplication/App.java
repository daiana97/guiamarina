package com.example.myapplication;


import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.util.Log;

import com.example.myapplication.Utils.ControllerConnection;
import com.example.myapplication.Utils.ForegroundService;
import com.example.myapplication.Utils.SharedPrefUtils;


public class App extends Application {

    public static final String CHANNEL_ID = "proyectoSubApp";
    private static App application;
    private boolean goodConex;
    private ControllerConnection ctrlConnection;
    private int connection;
    private final int ALLCONNECTION = 3;
    private NetworkInfo activeNetwork;

    public int getConnection() {
        return connection;
    }

    public void setConnection(int connection) {
        this.connection = connection;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        init();
        createNotificationChannel();

    }

    private void init() {
        application = this;
        ctrlConnection = new ControllerConnection(this);
        this.connection = ALLCONNECTION;
        ConnectivityManager connectivityManager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        ;
        activeNetwork = connectivityManager.getActiveNetworkInfo();
    }

    public static App getInstance() {
        return application;
    }


    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Guia Fauna Marina",
                    NotificationManager.IMPORTANCE_DEFAULT
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

    public void setGoodConecction(boolean goodConex) {
        this.goodConex = goodConex;
        if (this.goodConex) {
            Intent servicesIntent = new Intent(this, ForegroundService.class);
            boolean connectionGood = ctrlConnection.comprobarBuenaConexion();
            if (connectionGood) {
                if (isConnection())
                    startService(servicesIntent);
            }
        }

    }

    public boolean isConnection() {
        String userConnection = SharedPrefUtils.get(getApplicationContext(), "connection");

        Log.d("GETTYPE", String.valueOf(activeNetwork.getType()));

        if (activeNetwork.getType() == Integer.valueOf(userConnection)) return true;
        return userConnection.equals(String.valueOf(ALLCONNECTION));
    }
}
