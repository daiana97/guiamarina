package com.example.myapplication.Adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.models.Games;
import com.example.myapplication.models.SimpleDialog;

import java.util.List;

public class RecycleViewListGame extends RecyclerView.Adapter {

    private FragmentActivity fragmentActivity;
    private Context context;
    private List<Games> listgames;
    private Games dir;

    public RecycleViewListGame(FragmentActivity fragmentActivity, Context context, List<Games> listgames) {
        this.fragmentActivity = fragmentActivity;
        this.context = context;
        this.listgames = listgames;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(context).inflate(R.layout.item_game, null);
        return new Holder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        dir = listgames.get(position);
        Holder holder1;

        holder1 = (Holder) holder;
        if (dir.getLabel().equals("Challenge")) {
            holder1.textView.setText(dir.getChallenge().getName());

        } else {
            holder1.textView.setText(dir.getTrivia().getName());
        }

        holder1.button.setOnClickListener(new View.OnClickListener() {
            SimpleDialog mostrar;

            @Override
            public void onClick(View view) {
                Log.d("POSITION ", Integer.toString(position));
                dir = listgames.get(position);

                if (dir.getLabel().equals("Challenge")) {

                    mostrar = new SimpleDialog();
                    mostrar.setIdGame(dir.getChallenge().getName());
                    mostrar.setChallenge(dir.getChallenge());
                    mostrar.setLabelGame(dir.getLabel());
                    mostrar.setTituloDelJuego(dir.getChallenge().getName());
                    mostrar.setDescripcionDelJuego(dir.getChallenge().getDescription());
                    mostrar.setPicCount(dir.getChallenge().getPicCount());
                    mostrar.show(fragmentActivity.getSupportFragmentManager(), "SimpleDialog");
                } else {

                    mostrar = new SimpleDialog();
                    mostrar.setIdGame(dir.getTrivia().getName());
                    mostrar.setTrivia(dir.getTrivia());
                    mostrar.setTituloDelJuego(dir.getTrivia().getName());
                    mostrar.setLabelGame(dir.getLabel());
                    mostrar.setDescripcionDelJuego(dir.getTrivia().getDescription());
                    mostrar.show(fragmentActivity.getSupportFragmentManager(), "SimpleDialog");
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return listgames.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextView textView;
        Button button;

        public Holder(@NonNull View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.name_game);
            button = itemView.findViewById(R.id.button_accept);
        }
    }


} // FIN DE LA CLASE
