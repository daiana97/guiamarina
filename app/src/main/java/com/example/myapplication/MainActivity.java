package com.example.myapplication;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.example.myapplication.DAO.LevelDAO;
import com.example.myapplication.Utils.DataBase;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.controllers.CatalogFragment;
import com.example.myapplication.controllers.DetailsProfileFragment;
import com.example.myapplication.controllers.GameFragment2;
import com.example.myapplication.controllers.HomeFragment;
import com.example.myapplication.controllers.LoginActivity;
import com.example.myapplication.models.Level;
import com.example.myapplication.models.UserMobile;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;


/**
 * public class MainActivity extends AppCompatActivity {
 * }
 **/

public class MainActivity extends AppCompatActivity {


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    private List<Level> leveles;
    private LevelDAO levelsDAO;
    private DataBase database;
    private ProgressDialog progressDialog;


    private boolean itemGuest = false;
    public static final int REQUEST_PERMISSION = 200;
    private TextView title, description;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fragment = new HomeFragment();
                    break;
                case R.id.navigation_catalog:
                    fragment = new CatalogFragment();

                    break;
                case R.id.navigation_game:


                    getLeveles();
                    fragment = new GameFragment2();


                    break;
                case R.id.navigation_profile:
                    //  fragment = new ProfileFragment();
                    traerDatos();
                    fragment = new DetailsProfileFragment();
                    break;
                case R.id.navigation_singOutAnonymous:
                    singOutAnonymous();
                    itemGuest = true;
                    break;
            }


            if (!itemGuest) {
                replaceFragment(fragment);
            }


            return true;
        }
    };

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_placeholder, fragment);
        fragmentTransaction.commit();
    }

    private void setInitialFragment() {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_fragment_placeholder, new HomeFragment());
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION
                    }, REQUEST_PERMISSION);
        }


        BottomNavigationView navView = findViewById(R.id.bottom_navigation);

        inicializarFirebase();
        configurationMenu(navView);

        setInitialFragment();
        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }


    public void configurationMenu(BottomNavigationView navView) {

        String id = SharedPrefUtils.get(getApplicationContext(), "id");

        if (id.equals("invitado")) {

            navView.getMenu().findItem(R.id.navigation_profile).setVisible(false);
            navView.getMenu().findItem(R.id.navigation_singOutAnonymous).setVisible(true);
        }
    }


    private void singOutAnonymous() {

        SharedPrefUtils.put(getApplicationContext(), "id", "null");
        startActivity(new Intent(getBaseContext(), LoginActivity.class)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));

    }

    private void inicializarFirebase() {


        FirebaseApp.initializeApp(getApplicationContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        database = Room.databaseBuilder(getApplicationContext(), DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();

        levelsDAO = database.getLevelDAO();

    }

    public void getLeveles() {


        leveles = levelsDAO.getLevels();
        Log.d("ENtro en getLevels", "kjash");

        databaseReference.child("Levels").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                leveles.clear();
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {

                    Level level = objSnaptshot.getValue(Level.class);
                    leveles.add(level);

                    Log.d("NIVEL AGREGADO: ", level.toString());

                }

                if (leveles.size() > 0) {
                    Log.d("NO conexion", "carga por no conexion 11");
                    levelsDAO.nukeTable();
                    for (int i = 0; i < leveles.size(); i++) {
                        levelsDAO.insert(leveles.get(i));
                    }
                }

                Log.d("NO conexion aca", "carga por no conexion");
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                if (leveles.size() == 0) {
                    leveles = levelsDAO.getLevels();
                }

            }
        });


    } // fin getLeveles

    private void traerDatos() {


        final String id = SharedPrefUtils.get(getApplicationContext(), "id");

        databaseReference.child("UserMobile").child(id).addListenerForSingleValueEvent((new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                UserMobile user = dataSnapshot.getValue(UserMobile.class);
                SharedPrefUtils.put(getApplicationContext(), "point", String.valueOf(user.getPoint()));
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        }));


    }

    private void progressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void onCancelDialog() {
        progressDialog.cancel();
    }


} // FIN DE LA CLASE
