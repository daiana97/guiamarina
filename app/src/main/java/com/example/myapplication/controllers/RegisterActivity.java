package com.example.myapplication.controllers;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.models.UserMobile;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class RegisterActivity extends AppCompatActivity {

    private static final String KEY_EMPTY = "";

    private Button btnLogin;
    private Button btnRegister;

    private EditText etName;
    private EditText etSurname;
    private EditText etUsername;
    private EditText etEmail;
    private EditText etPassword;
    private EditText etConfirmPassword;

    private ProgressDialog pDialog;


    private String name;
    private String surname;
    private String username;
    private String email;
    private String password;
    private String confirmPassword;


    private FirebaseAuth mAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    private Button.OnClickListener clickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
        }
    };

    private Button.OnClickListener clickRegister = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            name = etName.getText().toString().toLowerCase().trim();
            surname = etSurname.getText().toString().toLowerCase().trim();
            username = etUsername.getText().toString().toLowerCase().trim();
            email = etEmail.getText().toString().trim();
            password = etPassword.getText().toString().trim();
            confirmPassword = etConfirmPassword.getText().toString().trim();

            if (validateInputs()) {

                displayLoader();
                registerUser();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);


        mAuth = FirebaseAuth.getInstance();

        inicializarFirebase();
        init();


        btnLogin.setOnClickListener(clickLogin);
        btnRegister.setOnClickListener(clickRegister);

    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }


    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user) {

        if (user != null) {
            Log.d("USER email: ", user.getEmail());
        }
    }

    private void createUserMobile(String id) {


        SharedPrefUtils.put(getApplicationContext(), "id", id);
        SharedPrefUtils.put(getApplicationContext(), "name", name);
        SharedPrefUtils.put(getApplicationContext(), "surname", surname);
        SharedPrefUtils.put(getApplicationContext(), "username", username);
        SharedPrefUtils.put(getApplicationContext(), "point", "0");
        SharedPrefUtils.put(getApplicationContext(), "email", email);


        // se crea el usuario

        UserMobile p = new UserMobile();
        p.setId(id);
        p.setName(name);
        p.setSurname(surname);
        p.setUsername(username);
        p.setPoint(0);
        p.setEmail(email);
        p.setPassword(password);
        databaseReference.child("UserMobile").child(p.getId()).setValue(p);

        Toast.makeText(this, "Cuenta creada", Toast.LENGTH_LONG).show();
        limpiarCajas();


    }

    private void init() {


        btnLogin = findViewById(R.id.btnRegisterLogin);
        btnRegister = findViewById(R.id.btnRegister);

        etName = findViewById(R.id.etName);
        etSurname = findViewById(R.id.etSurname);
        etUsername = findViewById(R.id.etUsername);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);

        pDialog = new ProgressDialog(RegisterActivity.this);


    }

    private void limpiarCajas() {
        etName.setText("");
        etSurname.setText("");
        etUsername.setText("");
        etEmail.setText("");
        etPassword.setText("");
        etConfirmPassword.setText("");
    }


    private boolean validateInputs() {


        if (KEY_EMPTY.equals(name)) {
            etName.setError(getResources().getString(R.string.name_empty));
            etName.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(surname)) {
            etSurname.setError(getResources().getString(R.string.surname_empty));
            etSurname.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(username)) {
            etUsername.setError(getResources().getString(R.string.username_empty));
            etUsername.requestFocus();
            return false;
        }
        if (KEY_EMPTY.equals(email)) {
            etEmail.setError(getResources().getString(R.string.email_empty));
            etEmail.requestFocus();
            return false;

        }
        if (KEY_EMPTY.equals(password)) {
            etPassword.setError(getResources().getString(R.string.password_empty));
            etPassword.requestFocus();
            return false;
        }

        if (KEY_EMPTY.equals(confirmPassword)) {
            etConfirmPassword.setError(getResources().getString(R.string.passwordC_empty));
            etConfirmPassword.requestFocus();
            return false;
        }
        if (!password.equals(confirmPassword)) {
            etConfirmPassword.setError(getResources().getString(R.string.password_confirm));
            etConfirmPassword.requestFocus();
            return false;
        }

        return true;
    }


    private void registerUser() {

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("REGISTRADO", "createUserWithEmail:success");

                            String id = task.getResult().getUser().getUid();

                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);

                            createUserMobile(id);
                            loadDashboard();


                        } else {
                            // If sign in fails, display a message to the user.
                            onCancelled();
                            Log.w("FALLO", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "El email esta registrado.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }


                        // ...
                    }
                });


    }  // FIN REGISTERUSER

    private void displayLoader() {
        // ProgressDialog pDialog = new ProgressDialog(RegisterActivity.this);
        pDialog.setMessage(getResources().getString(R.string.singning_up));
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();
    }

    private void onCancelled() {
        pDialog.cancel();

    }


    private void loadDashboard() {

        onCancelled();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();

    }


} // FIN DE LA CLASE
