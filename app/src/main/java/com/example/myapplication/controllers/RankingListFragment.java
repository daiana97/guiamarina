package com.example.myapplication.controllers;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.Adapters.RecycleViewItem;
import com.example.myapplication.R;
import com.example.myapplication.models.UserMobile;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class RankingListFragment extends Fragment {

    private View rankingView;
    private List<UserMobile> places;

    private RecyclerView recyclerView;
    private RecycleViewItem adapter;


    private ProgressDialog progressDialog;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rankingView = inflater.inflate(R.layout.fragment_ranking, container, false);


        init();
        inicializarFirebase();
        progressDialog();
        listarUsuarios();
        onCancelDialog();


        return rankingView;
    }


    private void init() {
        TextView list = (TextView) rankingView.findViewById(R.id.titleListUsers);
        recyclerView = rankingView.findViewById(R.id.top_list);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(rankingView.getContext());
        recyclerView.setLayoutManager(manager);
        places = new ArrayList<>();
    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this.getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

    }

    private void listarUsuarios() {


        databaseReference.child("UserMobile").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {

                    UserMobile userMobile = objSnaptshot.getValue(UserMobile.class);
                    places.add(userMobile);

                    firstPlaces(places);
                    if (places.size() > 0) {
                        adapter = new RecycleViewItem(rankingView.getContext(), places);
                        recyclerView.setAdapter(adapter);
                    }

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void firstPlaces(List<UserMobile> places) {

        Collections.sort(places, new Comparator<UserMobile>() {
            @Override
            public int compare(UserMobile userMobile, UserMobile t1) {
                return Integer.compare(userMobile.getPoint(), t1.getPoint()) * -1;
            }
        });

    }

    private void progressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void onCancelDialog() {
        progressDialog.cancel();
    }


}
