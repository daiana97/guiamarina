package com.example.myapplication.controllers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.models.UserMobile;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class DetailsProfileFragment extends Fragment {


    private View viewDetailsProfile;
    private FloatingActionButton configurationButton;
    private TextView txtName;
    private TextView txtSurname;
    private TextView txtUsername;
    private TextView txtEmail;
    private TextView txtPts;
    private TextView txtNameEdit;
    private TextView txtSurnameEdit;
    private TextView txtUsernameEdit;
    private TextView txtEmailEdit;
    private TextView txtPtsEdit;


    private Button editProfile;
    private Button saveProfile;
    private Button cancelEditProfile;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    private Button.OnClickListener bOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            configuration();
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewDetailsProfile = inflater.inflate(R.layout.details_profile, container, false);


        inicializarFirebase();

        init();


        configurationButton.setOnClickListener(bOnClickListener);

        editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activarEdit();
            }
        });

        cancelEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cancelEdit();
            }
        });

        saveProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                modificarDatos();
            }
        });


        return viewDetailsProfile;
    }

    private void init() {


        txtName = viewDetailsProfile.findViewById(R.id.textNameUser);
        txtSurname = viewDetailsProfile.findViewById(R.id.textSurnameUser);
        txtUsername = viewDetailsProfile.findViewById(R.id.textUsernameUser);
        txtEmail = viewDetailsProfile.findViewById(R.id.textEmailUser);

        txtNameEdit = viewDetailsProfile.findViewById(R.id.textNameUserEdit);
        txtSurnameEdit = viewDetailsProfile.findViewById(R.id.textSurnameUserEdit);
        txtUsernameEdit = viewDetailsProfile.findViewById(R.id.textUsernameUserEdit);
        txtEmailEdit = viewDetailsProfile.findViewById(R.id.textEmailUserEdit);
        txtPtsEdit = viewDetailsProfile.findViewById(R.id.textPointsUserEdit);


        txtPts = viewDetailsProfile.findViewById(R.id.textPointsUser);
        configurationButton = viewDetailsProfile.findViewById(R.id.button_Configuration);

        editProfile = viewDetailsProfile.findViewById(R.id.buttonEdit);
        saveProfile = viewDetailsProfile.findViewById(R.id.buttonSave);
        cancelEditProfile = viewDetailsProfile.findViewById(R.id.buttonCancel);


        traerDatos();

        cancelEdit();

    }

    public void configuration() {
        Fragment fragment = new ProfileFragment();
        replaceFragment(fragment);
    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }


    public void activarEdit() {

        editProfile.setVisibility(View.INVISIBLE);
        saveProfile.setVisibility(View.VISIBLE);
        cancelEditProfile.setVisibility(View.VISIBLE);


        txtNameEdit.setFocusable(true);
        txtNameEdit.setEnabled(true);

        txtSurnameEdit.setFocusable(true);
        txtSurnameEdit.setEnabled(true);

        // txtUsernameEdit.setFocusable(true);
        //  txtUsernameEdit.setEnabled(true);

        //  txtEmailEdit.setFocusable(true);
        //  txtEmailEdit.setEnabled(true);

    }

    public void cancelEdit() {

        editProfile.setVisibility(View.VISIBLE);
        editProfile.setEnabled(true);

        saveProfile.setVisibility(View.INVISIBLE);
        cancelEditProfile.setVisibility(View.INVISIBLE);

        txtNameEdit.setEnabled(false);
        txtSurnameEdit.setEnabled(false);
        txtUsernameEdit.setEnabled(false);
        txtEmailEdit.setEnabled(false);
        txtPtsEdit.setEnabled(false);


    }

    public void traerDatos() {
        txtNameEdit.setText(SharedPrefUtils.get(getActivity().getApplicationContext(), "name"));
        txtSurnameEdit.setText(SharedPrefUtils.get(getActivity().getApplicationContext(), "surname"));
        txtUsernameEdit.setText(SharedPrefUtils.get(getActivity().getApplicationContext(), "username"));
        txtEmailEdit.setText(SharedPrefUtils.get(getActivity().getApplicationContext(), "email"));
        txtPtsEdit.setText(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));
    }


    public void modificarDatos() {

        String id = SharedPrefUtils.get(getActivity().getApplicationContext(), "id");

        String name = txtNameEdit.getText().toString();
        String surname = txtSurnameEdit.getText().toString();
        String username = txtUsernameEdit.getText().toString();
        String email = txtEmailEdit.getText().toString();
        int point = Integer.valueOf(txtPtsEdit.getText().toString());


        SharedPrefUtils.put(getActivity().getApplicationContext(), "name", name);
        SharedPrefUtils.put(getActivity().getApplicationContext(), "surname", surname);
        SharedPrefUtils.put(getActivity().getApplicationContext(), "username", username);


        UserMobile p = new UserMobile();
        p.setId(id);
        p.setName(name);
        p.setSurname(surname);
        p.setUsername(username);
        p.setEmail(email);
        p.setPoint(point);


        databaseReference.child("UserMobile").child(p.getId()).setValue(p);

        cancelEdit();
        Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.details_editOK, Toast.LENGTH_SHORT);
        toast.show();


    } // fin del modificar datos


    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


} // FIN DE LA CLASE
