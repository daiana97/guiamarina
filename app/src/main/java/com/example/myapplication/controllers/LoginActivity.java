package com.example.myapplication.controllers;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.myapplication.MainActivity;
import com.example.myapplication.R;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.models.UserMobile;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class LoginActivity extends AppCompatActivity {

    private Button btnRegister;
    private Button btnLogin;
    private Button btnGuest;
    private EditText etUsername;
    private EditText etPassword;


    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();


        btnRegister = findViewById(R.id.btnLoginRegister);
        btnLogin = findViewById(R.id.btnLogin);
        etUsername = findViewById(R.id.etLoginUsername);
        etPassword = findViewById(R.id.etLoginPassword);
        btnGuest = findViewById(R.id.login_guest);


        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

                if (firebaseAuth.getCurrentUser() != null) {

                    //  startActivity(new Intent(LoginActivity.this,MainActivity.class));
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    startActivity(i);
                }


            }
        };


        btnGuest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                anonymous();
            }

        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
                finish();
            }
        });


    }


    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user) {


    }

    private void signOut() {
        mAuth.signOut();
        updateUI(null);
    }

    private void anonymous() {
        SharedPrefUtils.put(getApplicationContext(), "username", "null");
        SharedPrefUtils.put(getApplicationContext(), "password", "null");
        SharedPrefUtils.put(getApplicationContext(), "name", "null");
        SharedPrefUtils.put(getApplicationContext(), "surname", "null");
        SharedPrefUtils.put(getApplicationContext(), "email", "null");
        SharedPrefUtils.put(getApplicationContext(), "point", "null");
        SharedPrefUtils.put(getApplicationContext(), "connection", "3");
        SharedPrefUtils.put(getApplicationContext(), "id", "invitado");
        loadDashboard();


    }

    private void login() {
        SharedPrefUtils.put(getApplicationContext(), "id", "null");

        if (!validateForm()) {
            return;
        }

        String email = etUsername.getText().toString();
        String password = etPassword.getText().toString();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN ADENTRO", "signInWithEmail:success");

                            String id = task.getResult().getUser().getUid();
                            guardarDatos(id);


                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);


                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("FALLO", "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });


    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this);
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

    }


    private void guardarDatos(final String id) {


        inicializarFirebase();


        // databaseReference.child("UserMobile").addValueEventListener(new ValueEventListener() {

        databaseReference.child("UserMobile").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {
                    UserMobile p = objSnaptshot.getValue(UserMobile.class);


                    if (p.getId().equals(id)) {

                        SharedPrefUtils.put(getApplicationContext(), "id", p.getId());
                        SharedPrefUtils.put(getApplicationContext(), "name", p.getName());
                        SharedPrefUtils.put(getApplicationContext(), "surname", p.getSurname());
                        SharedPrefUtils.put(getApplicationContext(), "username", p.getUsername());
                        SharedPrefUtils.put(getApplicationContext(), "point", String.valueOf(p.getPoint()));
                        SharedPrefUtils.put(getApplicationContext(), "email", p.getEmail());
                    }

                }
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        databaseReference.onDisconnect();
    }


    private boolean validateForm() {
        boolean valid = true;

        String email = etUsername.getText().toString();
        if (TextUtils.isEmpty(email)) {
            etUsername.setError("Required.");
            valid = false;
        } else {
            etUsername.setError(null);
        }

        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(password)) {
            etPassword.setError("Required.");
            valid = false;
        } else {
            etPassword.setError(null);
        }

        return valid;
    }


    private void loadDashboard() {

        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(i);
        finish();


    }


}

