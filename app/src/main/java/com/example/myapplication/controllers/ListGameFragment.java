package com.example.myapplication.controllers;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.myapplication.Adapters.RecycleViewListGame;
import com.example.myapplication.DAO.ChallengeDAO;
import com.example.myapplication.DAO.OptionDAO;
import com.example.myapplication.DAO.TriviaDAO;
import com.example.myapplication.R;
import com.example.myapplication.Utils.DataBase;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.models.Challenge;
import com.example.myapplication.models.Games;
import com.example.myapplication.models.Option;
import com.example.myapplication.models.Trivia;
import com.example.myapplication.models.UserMobile;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ListGameFragment extends Fragment {


    private View listGame;
    private String level;
    private List<Games> games;
    private RecyclerView recyclerView;
    private RecycleViewListGame adapter;
    private ProgressDialog progressDialog;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;

    private OptionDAO optionsDAO;
    private ChallengeDAO challengeDAO;
    private TriviaDAO triviaDAO;
    private List<Challenge> challengesDB;
    private List<Trivia> triviasDB;
    private DataBase database;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        listGame = inflater.inflate(R.layout.fragment_list_game, container, false);


        init();
        inicializarFirebase();


        String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

        if (!user.equals("null")) {
            modificarDatos();
        }

        loadGames();

        return listGame;
    }

    private void init() {
        games = new ArrayList<>();

        recyclerView = listGame.findViewById(R.id.list_games);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(listGame.getContext());
        recyclerView.setLayoutManager(manager);


        database = Room.databaseBuilder(getContext(), DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();

        optionsDAO = database.getOptionDAO();
        challengeDAO = database.getChallengeDAO();
        triviaDAO = database.getTriviaDAO();


    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this.getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

    }

    private void progressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void onCancelDialog() {
        progressDialog.cancel();
    }

    public void getChallenges() {

        //se limpia la lista games
        games.clear();
        //  databaseReference.child("Challenges").addValueEventListener(new ValueEventListener() {

        databaseReference.child("Challenges").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                //limpiar lista games
                games.clear();
                challengeDAO.nukeTable();

                //borrar datos de db


                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {


                    Games game = new Games();

                    Challenge challenge = objSnaptshot.getValue(Challenge.class);
                    Log.d("Juego TRAIDO: ", challenge.toString());

                    if (challenge.getLevel_id().equals(getLevel())) {

                        challengeDAO.insert(challenge);

                        game.setChallenge(challenge);
                        game.setLabel("Challenge");
                        games.add(game);
                    }

                    Log.d("Juego AGREGADO: ", challenge.toString());


                }
                //insertar en db
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                //traer datos de db
                //guardar en lista games
            }
        });


        if (games.size() == 0) {
            challengesDB = challengeDAO.getChallengesByLevel(getLevel());

            for (Challenge challenge : challengesDB
            ) {
                Games game = new Games();
                game.setChallenge(challenge);
                game.setLabel("Challenge");
                games.add(game);
            }

        }


        // traer trivias
        getTrivias();

    }  // FIN GET CHALLENGES

    public void getTrivias() {


        // databaseReference.child("Trivias").addValueEventListener(new ValueEventListener() {
        databaseReference.child("Trivias").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                triviaDAO.nukeTable();
                optionsDAO.nukeTable();

                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {


                    Games game = new Games();

                    Trivia trivia = objSnaptshot.getValue(Trivia.class);

                    for (Option op : trivia.getOptions()) {
                        op.setTriviaId(trivia.getName());
                        optionsDAO.insert(op);
                    }

                    if (trivia.getLevel_id().equals(getLevel())) {

                        triviaDAO.insert(trivia);

                        game.setTrivia(trivia);
                        game.setLabel("Trivia");
                        games.add(game);
                    }


                    Log.d("Juego AGREGADO: ", trivia.toString());

                    adapter = new RecycleViewListGame(getActivity(), listGame.getContext(), games) {
                    };
                    if (adapter.getItemCount() > 0) {
                        recyclerView.setAdapter(adapter);
                    }

                }
                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        int countChallenges = challengesDB.size();
        Log.d("cantidad de chall en db", String.valueOf(countChallenges));

        if (games.size() == countChallenges) {

            Log.d("entro aca ", String.valueOf(countChallenges));


            triviasDB = triviaDAO.getTriviaByLevel(getLevel());

            for (Trivia trivia : triviasDB) {

                trivia.setOptions(optionsDAO.getOptionsByTrivia(trivia.getName()));
                Games game = new Games();
                game.setTrivia(trivia);
                game.setLabel("Trivia");
                games.add(game);
            }
            adapter = new RecycleViewListGame(getActivity(), listGame.getContext(), games) {
            };
            if (adapter.getItemCount() > 0) {
                recyclerView.setAdapter(adapter);
            }


        }


    } // fin getTrivias


    public void modificarDatos() {

        String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");
        String name = SharedPrefUtils.get(getActivity().getApplicationContext(), "name");
        String surname = SharedPrefUtils.get(getActivity().getApplicationContext(), "surname");
        String email = SharedPrefUtils.get(getActivity().getApplicationContext(), "email");
        int point = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));
        String id = SharedPrefUtils.get(getActivity().getApplicationContext(), "id");

        UserMobile userMobile = new UserMobile();
        userMobile.setId(id);
        userMobile.setName(name);
        userMobile.setUsername(user);
        userMobile.setSurname(surname);
        userMobile.setEmail(email);
        userMobile.setPoint(point);

        databaseReference.child("UserMobile").child(userMobile.getId()).setValue(userMobile);
        //   Toast.makeText(this,"Actualizado", Toast.LENGTH_LONG).show();


    } // fin del modificar datos


    public String getLevel() {
        return this.level;
    }

    public void setLevel(String level) {
        this.level = level;
    }


    private void loadGames() {
        progressDialog();
        getChallenges();
        onCancelDialog();
    }

} // FIN DE LA CLASE
