package com.example.myapplication.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.example.myapplication.DAO.GroupDAO;
import com.example.myapplication.DAO.SpecieDAO;
import com.example.myapplication.R;
import com.example.myapplication.Utils.DataBase;
import com.example.myapplication.models.Group;
import com.example.myapplication.models.Specie;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SpecieFragment extends Fragment {


    private View infoGroup;
    private List<Specie> species;
    private Group group;
    private SpecieAdapter specieAdapter;
    private GridView gridView;
    private TextView nameG;
    private TextView descriptionG;


    private DataBase database;
    private SpecieDAO specieDao;
    private GroupDAO groupDao;
    private List<Specie> speciesdb;


    private ProgressDialog progressDialog;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        infoGroup = inflater.inflate(R.layout.fragment_specie, container, false);

        init();

        onCancelDialog();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                detailsSpecie(i);
            }
        });


        return infoGroup;

    }

    private void progressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }


    private void onCancelDialog() {
        progressDialog.cancel();
    }

    private void init() {
        progressDialog();

        database = Room.databaseBuilder(getContext(), DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        specieDao = database.getSpecieDAO();
        groupDao = database.getGroupDAO();


        this.species = new ArrayList<>();
        specieAdapter = new SpecieAdapter();

        gridView = infoGroup.findViewById(R.id.gridview);
        nameG = (TextView) infoGroup.findViewById(R.id.nameGroup);
        descriptionG = (TextView) infoGroup.findViewById(R.id.descriptionGroup);


        inicializarFirebase();

        // listarEspecies();
        listarEspecies2();
        noConexion();


    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this.getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();

    }

    private void setaerGrupo() {
        nameG.setText(group.getName());
        descriptionG.setText(group.getDescription());
    }


    /**
     * private void listarEspecies() {
     * <p>
     * databaseReference.child("Species").addValueEventListener(new ValueEventListener() {@Override
     * public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
     * <p>
     * Log.d("image","llamada");
     * for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
     * <p>
     * Specie specie = objSnaptshot.getValue(Specie.class);
     * if(specie.getGroup_id().equals(getGroup().getName())){
     * traerImagen(specie);
     * }
     * <p>
     * }
     * }
     *
     * @Override public void onCancelled(@NonNull DatabaseError databaseError) {
     * <p>
     * }
     * });
     * <p>
     * databaseReference.child("Species").addValueEventListener(new ValueEventListener() {
     * @Override public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
     * <p>
     * species.clear();
     * specieDao.nukeTable();
     * <p>
     * Log.d("VOlvio a realizar","llamada");
     * for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()){
     * <p>
     * <p>
     * Specie specie = objSnaptshot.getValue(Specie.class);
     * specieDao.insert(specie);
     * <p>
     * String nameFile = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/" + specie.getImage() + ".jpg";
     * specie.setImage(nameFile);
     * specieDao.update(specie);
     * <p>
     * <p>
     * if(specie.getGroup_id().equals(getGroup().getName())){
     * species.add(specie);
     * <p>
     * if (specieAdapter.getCount() > 0) {
     * gridView.setAdapter(specieAdapter);
     * }
     * <p>
     * }
     * <p>
     * nameG.setText(group.getName());
     * descriptionG.setText(group.getDescription());
     * <p>
     * }
     * }
     * @Override public void onCancelled(@NonNull DatabaseError databaseError) {
     * group = groupDao.getGroupByName((String) group.getName());
     * <p>
     * species = specieDao.getSpecieByGroup(group.getName());
     * if (specieAdapter.getCount() > 0) {
     * gridView.setAdapter(specieAdapter);
     * }
     * <p>
     * nameG.setText(group.getName());
     * descriptionG.setText(group.getDescription());
     * }
     * });
     * <p>
     * <p>
     * }
     */

    private void listarEspecies2() {

        databaseReference.child("Species").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                Log.d("image", "llamada");
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {

                    Specie specie = objSnaptshot.getValue(Specie.class);
                    if (specie.getGroup_id().equals(getGroup().getName())) {
                        traerImagen(specie);
                    }

                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        databaseReference.child("Species").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                species.clear();
                specieDao.nukeTable();

                Log.d("VOlvio a realizar", "llamada");
                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {


                    Specie specie = objSnaptshot.getValue(Specie.class);
                    specieDao.insert(specie);

                    String nameFile = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/" + specie.getImage() + ".jpg";
                    specie.setImage(nameFile);
                    specieDao.update(specie);


                    if (specie.getGroup_id().equals(getGroup().getName())) {
                        species.add(specie);

                        if (specieAdapter.getCount() > 0) {
                            gridView.setAdapter(specieAdapter);
                        }

                    }

                    nameG.setText(group.getName());
                    descriptionG.setText(group.getDescription());

                }
                databaseReference.removeEventListener(this);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                group = groupDao.getGroupByName((String) group.getName());

                species = specieDao.getSpecieByGroup(group.getName());
                if (specieAdapter.getCount() > 0) {
                    gridView.setAdapter(specieAdapter);
                }

                nameG.setText(group.getName());
                descriptionG.setText(group.getDescription());
            }
        });


    }

    private void noConexion() {

        if (species.size() == 0) {

            Log.d("entra ACA 1", "sin conexion, no peticion");
            species = specieDao.getSpecieByGroup(getGroup().getName());


            if (specieAdapter.getCount() > 0) {
                gridView.setAdapter(specieAdapter);
            }
            nameG.setText(group.getName());
            descriptionG.setText(group.getDescription());

        }

    }


    private void traerImagen(final Specie s) {

        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReferenceFromUrl("gs://pruebafirebase-b3c3b.appspot.com").child("Species/" + s.getImage());

        String nameImage = "";

        try {
            final File localFile = File.createTempFile("images", "jpg");
            storageRef.getFile(localFile).addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {

                    Log.d("ENTRA ACA", "jkah");
                    Bitmap bitmap = BitmapFactory.decodeFile(localFile.getAbsolutePath());

                    // primero borro las imagenes viejas y desp las vuelvo a guardar
                    String nameFile = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES + "/" + s.getImage() + ".jpg";
                    File file = new File(nameFile);
                    FileOutputStream fOut;

                    try {
                        fOut = new FileOutputStream(file);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fOut);

                        // Llamar aflush() obliga a “vaciar” el búfer, lo que significa que escribirá..
                        fOut.flush();
                        fOut.close();

                        // se llama para liberar la memoria
                        //   bitmap.recycle();

                    } catch (Exception e) {

                    }


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    Log.d("entra por aca 22", "no hay conexion");

                }
            });

        } catch (IOException e) {
            Log.d("entra por aca", "no hay conexion");

        }

    }


    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void detailsSpecie(int position) {
        Fragment fragment = new DetailsSpecieFragment();
        ((DetailsSpecieFragment) fragment).setSpecie(species.get(position));
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    /**********************************************************************************************/

    public class SpecieAdapter extends BaseAdapter {


        private LayoutInflater mInflater;


        private Specie specie;

        SpecieAdapter() {
            mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        }


        public int getCount() {
            return species.size();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {


            specie = species.get(position);
            final ViewHolder holder;


            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.shr_specie_card, null);

                holder.tittle = (TextView) convertView.findViewById(R.id.titleSpecie);

                holder.subtittle = (TextView) convertView.findViewById(R.id.subtitleSpecie);
                holder.specieImage = (ImageView) convertView.findViewById(R.id.imageSpecie);

                holder.tittle.setText(specie.getName());
                holder.subtittle.setText(specie.getScientificName());

                Bitmap bitmap = BitmapFactory.decodeFile(specie.getImage());
                holder.specieImage.setImageBitmap(bitmap);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            return convertView;
        }
    }


    class ViewHolder {


        ImageView specieImage;
        TextView tittle;
        TextView subtittle;


    }


} // FIN DE LA CLASE

