package com.example.myapplication.controllers;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.example.myapplication.DAO.LevelDAO;
import com.example.myapplication.R;
import com.example.myapplication.Utils.DataBase;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.models.Level;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class GameFragment2 extends Fragment {


    private LevelAdapter levelAdapter;
    private SharedPreferences preferences;
    private List<Level> leveles;
    private GridView gridView;
    private TextView points;
    private FloatingActionButton rankingButton;
    private View gameView;


    private LevelDAO levelsDAO;
    private DataBase database;
    private boolean noConexion = false;

    private ProgressDialog progressDialog;

    private Button.OnClickListener bOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            rankingList();
        }
    };


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        gameView = inflater.inflate(R.layout.game_fragment, container, false);


        init(gameView);

        traerLeveles();


        Log.d("finalizo traido", "niveles");


        rankingButton.setOnClickListener(bOnClickListener);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                int position = i;

                if (position == 0) {
                    preferences.edit().putString("done" + position, "yes").apply();
                }

                if (preferences.getString("done" + position, null) != null && preferences.getString("done" + position, null).equals("yes")) {
                    view.setClickable(true);
                    view.setEnabled(true);
                    listGames(i);
                } else {


                    view.setClickable(false);
                    view.setEnabled(false);
                }


            }
        });

        return gameView;
    }

    private void init(View gameView) {


        preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        levelAdapter = new LevelAdapter();
        leveles = new ArrayList<>();
        gridView = gameView.findViewById(R.id.gridview);
        rankingButton = gameView.findViewById(R.id.button_Ranking);

        database = Room.databaseBuilder(getContext(), DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();

        levelsDAO = database.getLevelDAO();


    }// fin init


    public void listGames(int position) {

        String levelName = leveles.get(position).getName();
        Fragment fragment = new ListGameFragment();
        ((ListGameFragment) fragment).setLevel(levelName);

        replaceFragment(fragment);
    }

    public void rankingList() {
        Fragment fragment = new RankingListFragment();
        replaceFragment(fragment);
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


    public void traerLeveles() {

        // progressDialog();
        //   final ProgressDialog dialog = ProgressDialog.show(getActivity(), getString(R.string.loading), getString(R.string.wait_please), true);

        if (leveles.size() == 0) {
            leveles = levelsDAO.getLevels();
            Log.d("NIveles guardados", "kjah");
            if (levelAdapter.getCount() > 0) {
                progressDialog();
                gridView.setAdapter(levelAdapter);
            }
        }


    } // fin getLeveles

    public List<Level> getLeveles() {
        return leveles;
    }

    public void setLeveles(List<Level> leveles) {
        this.leveles = leveles;
    }

    private void progressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void onCancelDialog() {
        progressDialog.cancel();
    }


    private void mostrarPts() {

        points = (TextView) gameView.findViewById(R.id.pointsUser);
        String ptsUser = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");

        if (ptsUser.equals("null")) {
            points.append(": 0 ");
        } else {
            points.append(": " + ptsUser);
        }

    }

    /** --------------------------------------------------------------------------- */
    /**********************************************************************************************/

    public class LevelAdapter extends BaseAdapter {

        public List<Integer> mProgressIds;
        private LayoutInflater mInflater;
        private boolean finish = false;

        public LevelAdapter() {
            mInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public int getCount() {
            return leveles.size();

        }

        public void finishAdapter() {
            onCancelDialog();
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }


        public View getView(int position, View convertView, ViewGroup parent) {


            ViewHolder holder;
            mProgressIds = new ArrayList<Integer>();
            Float percentage = 0.f;


            for (int i = 0; i < leveles.size(); i++) {
                int progress = leveles.get(i).getScoreRequired();
                mProgressIds.add(progress);
            }


            if (convertView == null) {
                holder = new ViewHolder();
                convertView = mInflater.inflate(
                        R.layout.item_leveles, null);
                holder.progressBar = (ProgressBar) convertView.findViewById(R.id.progress);
                holder.levelNumberTextView = (TextView) convertView.findViewById(R.id.levelItem);
                holder.scoreTextView = (TextView) convertView.findViewById(R.id.score);
                holder.lockImageView = (ImageView) convertView.findViewById(R.id.lock_imageView);
                convertView.setTag(holder);

                // CALCULA EL PORCENTAJE
                int max = leveles.get(position).getScoreRequired();

                String points = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");
                int pointUser = 0;

                if (points.equals("null")) {
                    pointUser = 0;
                    percentage = 0.f;
                } else {
                    percentage = (Float.valueOf(points) / max) * 100;
                    pointUser = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));

                }


                Log.d("POSITION", String.valueOf(position));
                preferences.edit().putString("done" + position, null).apply();


                if (position > 0) {
                    //detect if level opened


                    // ARREGLO PARA EL PORCENTAJE EN NEGATIVO

                    int ptsAccRequeridos = 0;
                    for (int i = 0; i < position; i++) {
                        ptsAccRequeridos = ptsAccRequeridos + mProgressIds.get(i);
                    }

                    Log.d("valor acc", String.valueOf(ptsAccRequeridos));

                    String p = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");

                    int ptsUser = 0;

                    if (p.equals("null")) {
                        ptsUser = 0;
                        //    if (ptsUser >= ptsAccRequeridos){
                        //        preferences.edit().putString("done" + position, "yes").apply();
                        //   }
                    } else {
                        ptsUser = Integer.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point"));

                        if (ptsUser >= ptsAccRequeridos) {
                            preferences.edit().putString("done" + position, "yes").apply();
                        }

                    }


                    // HASTA ACA


                    if (preferences.getString("done" + position, null) != null && preferences.getString("done" + position, null).equals("yes")) {

                        String pos = String.valueOf(position + 1);
                        Log.d("POSITION STRING", pos);

                        holder.levelNumberTextView.setText(pos);
                        holder.levelNumberTextView.setBackgroundResource(R.drawable.circle);
                        holder.lockImageView.setVisibility(View.GONE);

                        int acc = 0;
                        for (int i = 0; i < position; i++) {
                            acc = acc + mProgressIds.get(i);
                        }

                        Log.d("valor acc", String.valueOf(acc));

                        percentage = ((Float.valueOf(SharedPrefUtils.get(getActivity().getApplicationContext(), "point")) - acc) / max) * 100;

                        // desde aca
                        holder.progressBar.setVisibility(View.VISIBLE);
                        holder.scoreTextView.setVisibility(View.VISIBLE);

                        //AGREGADO
                        Log.d("VALOR ACTUAL PORC:", String.valueOf(percentage));

                        String progress = String.format(Locale.ENGLISH, "%.0f", percentage);


                        if (Integer.valueOf(progress) > holder.progressBar.getMax()) {
                            progress = String.format(Locale.ENGLISH, "%.0f", Float.valueOf(holder.progressBar.getMax()));
                            holder.progressBar.setProgress(Integer.valueOf(progress));
                        } else {
                            holder.progressBar.setProgress(Integer.valueOf(progress));
                        }


                        pos = String.valueOf(position + 1);
                        Log.d("POSITION STRING 2", pos);
                        holder.levelNumberTextView.setText(pos);
                        String progressString = String.valueOf(progress) + "%";
                        holder.scoreTextView.setText(progressString);


                        if (pointUser >= acc) {
                            preferences.edit().putString("done" + position, "yes").apply();
                        }


                    } else {
                        holder.levelNumberTextView.setText("");
                        holder.progressBar.setVisibility(View.INVISIBLE);
                        holder.scoreTextView.setVisibility(View.INVISIBLE);
                        holder.lockImageView.setVisibility(View.VISIBLE);
                        holder.levelNumberTextView.setBackgroundColor(getResources().getColor(android.R.color.transparent));

                    }


                    if (position == (leveles.size() - 1)) {
                        finishAdapter();
                        mostrarPts();
                    }


                } else {

                    holder.progressBar.setVisibility(View.VISIBLE);
                    holder.scoreTextView.setVisibility(View.VISIBLE);

                    //AGREGADO
                    String progress = String.format(Locale.ENGLISH, "%.0f", percentage);


                    if (Integer.valueOf(progress) > holder.progressBar.getMax()) {
                        progress = String.format(Locale.ENGLISH, "%.0f", Float.valueOf(holder.progressBar.getMax()));
                        holder.progressBar.setProgress(Integer.valueOf(progress));
                    } else {
                        holder.progressBar.setProgress(Integer.valueOf(progress));
                    }


                    String pos = String.valueOf(position + 1);

                    holder.levelNumberTextView.setText(pos);
                    String progressString = String.valueOf(progress) + "%";
                    holder.scoreTextView.setText(progressString);


                    if (pointUser >= mProgressIds.get(position)) {
                        preferences.edit().putString("done" + position, "yes").apply();
                    }

                }


                // fin del if convertView
            } else {
                holder = (ViewHolder) convertView.getTag();
            }


            return convertView;
        }

    }

    class ViewHolder {
        TextView levelNumberTextView, scoreTextView;
        ProgressBar progressBar;
        ImageView lockImageView;
    }


} //FIN DE LA CLASE

