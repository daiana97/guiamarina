package com.example.myapplication.controllers;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.example.myapplication.models.Option;
import com.example.myapplication.models.Trivia;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class TriviaFragment extends Fragment {


    private ProgressDialog progressDialog;


    private View triviaFragment;
    private List<RadioButton> radioButtonList;


    private int ptsTrivia;
    private Trivia trivia;
    private TextView questionT;

    private FloatingActionButton enviar;
    private RecyclerView recyclerView;
    private RadioGroup radioGroup;
    private RelativeLayout relativeLayout;

    private String optionSelected;


    private Button.OnClickListener bOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            AlertDialog.Builder builder = new AlertDialog.Builder(triviaFragment.getContext());
            builder.setTitle(R.string.answerGame);
            if (trivia.getOptCorrect().equals(optionSelected)) {

                String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

                if (user.equals("null")) {

                    builder.setMessage(R.string.answerOKGameNoUser);
                    builder.setPositiveButton(R.string.accept, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getFragmentManager().popBackStack();

                        }
                    });

                } else {
                    builder.setMessage(R.string.answerOKGame);
                    builder.setPositiveButton(R.string.takePoints, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {


                            puntuarUsuario();
                            getFragmentManager().popBackStack();
                        }
                    });
                }
            } else {


                String user = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

                if (user.equals("null")) {

                    builder.setMessage(R.string.answerIncorrectGameNoUser);
                    builder.setPositiveButton(R.string.noTryAgain, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getFragmentManager().popBackStack();

                        }
                    });

                } else {
                    builder.setMessage(R.string.answerIncorrectGame);
                    builder.setPositiveButton(R.string.tryAgain, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            quitarPuntos();
                        }
                    });

                    builder.setNegativeButton(R.string.noTryAgain, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            getFragmentManager().popBackStack();
                        }
                    });
                }
            }
            builder.show();
        }
    };

    private RadioGroup.OnCheckedChangeListener selectButton = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {
            RadioButton radioButton = triviaFragment.findViewById(i);
            optionSelected = (String) radioButton.getText();
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        triviaFragment = inflater.inflate(R.layout.fragment_trivia, container, false);

        init();


        getOptions();

        radioGroup.setOnCheckedChangeListener(selectButton);
        enviar.setOnClickListener(bOnClickListener);

        return triviaFragment;
    }


    private void init() {


        radioButtonList = new ArrayList<>();
        questionT = (TextView) triviaFragment.findViewById(R.id.questionTrivia);
        enviar = triviaFragment.findViewById(R.id.btn_send);
        radioGroup = new RadioGroup(triviaFragment.getContext());
        relativeLayout = triviaFragment.findViewById(R.id.optTrivia);


    }


    private void progressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void onCancelDialog() {
        progressDialog.cancel();
    }

    private void getOptions() {

        progressDialog();
        Log.d("JUGAR TRIVIA ELEGIDA:", getTrivia().toString());

        RadioButton radioButton;

        for (Option op : getTrivia().getOptions()) {

            radioButton = new RadioButton(triviaFragment.getContext());
            radioButton.setText(op.getOption());
            radioButtonList.add(radioButton);
        }


        questionT.setText(trivia.getQuestion());

        for (RadioButton rb : radioButtonList) {
            radioGroup.addView(rb);
        }
        relativeLayout.addView(radioGroup);


        onCancelDialog();

    }

    public void puntuarUsuario() {

        String puntosActualizados = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");
        int pts = Integer.valueOf(puntosActualizados);
        pts = pts + trivia.getPoint();

        SharedPrefUtils.put(getActivity().getApplicationContext(), "point", String.valueOf(pts));
        puntosActualizados = SharedPrefUtils.get(getActivity().getApplicationContext(), "point");
        Log.d("PUnTOS usuario: ", String.valueOf(puntosActualizados));

        Toast toast = Toast.makeText(getActivity().getApplicationContext(), R.string.pointsReceived, Toast.LENGTH_SHORT);
        toast.show();


    }


    private void quitarPuntos() {

        int ptsRestar = trivia.getPoint() / radioButtonList.size();
        Log.d("Puntos a restar: ", String.valueOf(ptsRestar));

        if (ptsTrivia < ptsRestar) {
            ptsTrivia = 0;
        } else {
            ptsTrivia = ptsTrivia - ptsRestar;

        }

    }


    public Trivia getTrivia() {
        return this.trivia;
    }

    public void setTrivia(Trivia trivia) {
        this.trivia = trivia;
    }

} // FIN DE LA CLASE
