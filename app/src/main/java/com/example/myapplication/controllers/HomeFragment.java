package com.example.myapplication.controllers;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.myapplication.R;

public class HomeFragment extends Fragment {

    private View homeFragment;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        homeFragment = inflater.inflate(R.layout.home_fragment, container, false);

        init();
        return homeFragment;
    }

    private void init() {
        TextView title = homeFragment.findViewById(R.id.welcome);
        TextView description = homeFragment.findViewById(R.id.description);
    }


}
