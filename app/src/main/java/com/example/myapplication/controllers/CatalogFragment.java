package com.example.myapplication.controllers;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.myapplication.Adapters.RecycleViewGroup;
import com.example.myapplication.DAO.GroupDAO;
import com.example.myapplication.R;
import com.example.myapplication.Utils.DataBase;
import com.example.myapplication.models.Group;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CatalogFragment extends Fragment {


    private View catalogView;
    private List<Group> listGroup;

    private RecyclerView recyclerView;
    private RecycleViewGroup adapter;
    private TextView groupsInv;


    private ProgressDialog progressDialog;

    private GroupDAO groupDAO;
    private DataBase database;


    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;


    public static CatalogFragment newInstance() {
        return new CatalogFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        catalogView = inflater.inflate(R.layout.catalog_fragment, container, false);

        init();


        return catalogView;
    }

    private void progressDialog() {
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle(getString(R.string.loading));
        progressDialog.setMessage(getString(R.string.wait_please));
        progressDialog.setCancelable(false);
        progressDialog.show();

    }

    private void init() {

        progressDialog();

        this.listGroup = new ArrayList<>();


        database = Room.databaseBuilder(getContext(), DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();

        groupDAO = database.getGroupDAO();

        recyclerView = catalogView.findViewById(R.id.group_list);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(catalogView.getContext());
        recyclerView.setLayoutManager(manager);
        groupsInv = (TextView) catalogView.findViewById(R.id.titleGroups);


        inicializarFirebase();
        //listarGrupos();

        listarGrupos2();


    }

    private void inicializarFirebase() {
        FirebaseApp.initializeApp(this.getContext());
        firebaseDatabase = FirebaseDatabase.getInstance();


        databaseReference = firebaseDatabase.getReference();


    }

    private void listarGrupos() {

        //limpiar lista games
        //borrar datos de db

        Log.d("LLAMADA", "jajd");
        listGroup.clear();


        databaseReference.child("Groups").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                listGroup.clear();
                groupDAO.nukeTable();

                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {

                    Log.d("LLAMADA entraa", "jajd");
                    Group group = objSnaptshot.getValue(Group.class);

                    groupDAO.insert(group);

                    listGroup.add(group);

                    adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), getListGroups());
                    recyclerView.setAdapter(adapter);


                    if (listGroup.size() > 0) {
                        groupsInv.setText(R.string.group_invertebrates);
                        progressDialog.cancel();
                    }


                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

                Log.d("ENTTRO aca:", "sg");
                //traer datos de db
                listGroup = groupDAO.getGroups();

                adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), listGroup);
                recyclerView.setAdapter(adapter);

                if (listGroup.size() > 0) {
                    groupsInv.setText(R.string.group_invertebrates);
                    progressDialog.cancel();
                }
                //guardar en lista games
            }
        });


        if (listGroup.size() == 0) {

            listGroup = groupDAO.getGroups();
            adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), listGroup);
            recyclerView.setAdapter(adapter);

            if (listGroup.size() > 0) {
                groupsInv.setText(R.string.group_invertebrates);
                progressDialog.cancel();
            }
        }

    }

    private void listarGrupos2() {
        Log.d("LLAMADA", "entra en una  unica llamada");
        listGroup.clear();

        databaseReference.child("Groups").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                listGroup.clear();
                groupDAO.nukeTable();

                for (DataSnapshot objSnaptshot : dataSnapshot.getChildren()) {

                    Log.d("LLAMADA una sola vez", "jajd");
                    Group group = objSnaptshot.getValue(Group.class);

                    groupDAO.insert(group);

                    listGroup.add(group);

                    adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), getListGroups());
                    recyclerView.setAdapter(adapter);


                    if (listGroup.size() > 0) {
                        groupsInv.setText(R.string.group_invertebrates);
                        progressDialog.cancel();
                    }


                }

                databaseReference.removeEventListener(this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.d("ENTTRO aca:", "s por errorg");
                //traer datos de db
                listGroup = groupDAO.getGroups();

                adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), listGroup);
                recyclerView.setAdapter(adapter);

                if (listGroup.size() > 0) {
                    groupsInv.setText(R.string.group_invertebrates);
                    progressDialog.cancel();
                }
                //guardar en lista games
            }
        });


        if (listGroup.size() == 0) {

            listGroup = groupDAO.getGroups();
            adapter = new RecycleViewGroup(getActivity(), catalogView.getContext(), listGroup);
            recyclerView.setAdapter(adapter);

            if (listGroup.size() > 0) {
                groupsInv.setText(R.string.group_invertebrates);
                progressDialog.cancel();
            }
        }


    }


    public List<Group> getListGroups() {
        return listGroup;
    }


} // FIN DE LA CLASE
