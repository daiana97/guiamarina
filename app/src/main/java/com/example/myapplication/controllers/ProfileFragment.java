package com.example.myapplication.controllers;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplication.R;
import com.example.myapplication.Utils.SharedPrefUtils;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Locale;

public class ProfileFragment extends Fragment {


    private View viewProfile;
    private Spinner spinnerLeng;
    private Spinner spinnerNetwork;

    private Button closeApp;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;


    private AdapterView.OnItemSelectedListener selectedLeng = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
            selectLanguage(i);
        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        viewProfile = inflater.inflate(R.layout.profile_fragment, container, false);

        mAuth = FirebaseAuth.getInstance();
        init();

//        spinnerLeng.setOnItemSelectedListener(selectedLeng);


        closeApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                signOff();
                startActivity(new Intent(getActivity().getBaseContext(), LoginActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));

                signOut();
                Toast.makeText(getContext(), "Seccion cerrada", Toast.LENGTH_SHORT).show();

            }
        });


        return viewProfile;
    }

    private void init() {
        //TextView txtLeng = viewProfile.findViewById(R.id.txt_i18n);
        //  TextView txtNetwork = viewProfile.findViewById(R.id.txt_network);
        closeApp = viewProfile.findViewById(R.id.buttonClose);

        // app = App.getInstance();
        //spinnerLeng = viewProfile.findViewById(R.id.sp_i18n);
        //spinnerNetwork = viewProfile.findViewById(R.id.sp_network);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.i18n, android.R.layout.simple_spinner_item);
//        spinnerLeng.setAdapter(adapter);


        /** ArrayAdapter<CharSequence> adapternet = ArrayAdapter.createFromResource(getActivity(), R.array.network, android.R.layout.simple_spinner_item);
         spinnerNetwork.setAdapter(adapternet);  **/
    }


    public void selectLanguage(int position) {
        Fragment fragment;
        switch (position) {
            case 1:
                Locale localizacion = new Locale("en", "En");
                Locale.setDefault(localizacion);
                Configuration config = new Configuration();
                config.locale = localizacion;
                getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());
                Intent g_en = new Intent(getActivity(), getClass());
                g_en.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                fragment = new ProfileFragment();
                replaceFragment(fragment);
                break;
            case 2:
                localizacion = new Locale("es", "ES");
                Locale.setDefault(localizacion);
                config = new Configuration();
                config.locale = localizacion;
                getActivity().getResources().updateConfiguration(config, getActivity().getResources().getDisplayMetrics());
                Intent g_es = new Intent(getActivity(), getClass());
                g_es.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                fragment = new ProfileFragment();
                replaceFragment(fragment);
                break;
            default:
                break;
        }
    }


    private void signOut() {
        mAuth.signOut();

    }


    public void signOff() {
        String username = SharedPrefUtils.get(getActivity().getApplicationContext(), "username");

        if (!username.equals("null")) {

            SharedPrefUtils.put(getActivity().getApplicationContext(), "username", "null");
            SharedPrefUtils.put(getActivity().getApplicationContext(), "password", "null");
            SharedPrefUtils.put(getActivity().getApplicationContext(), "id", "null");
            SharedPrefUtils.put(getActivity().getApplicationContext(), "name", "null");
            SharedPrefUtils.put(getActivity().getApplicationContext(), "surname", "null");
            SharedPrefUtils.put(getActivity().getApplicationContext(), "email", "null");
            SharedPrefUtils.put(getActivity().getApplicationContext(), "point", "null");

        }


    }

    /**
     * Remove all entries from the backStack of this fragmentManager.
     *
     * @param fragmentManager the fragmentManager to clear.
     */
    private void clearBackStack(FragmentManager fragmentManager) {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry entry = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(entry.getId(), FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_fragment_placeholder, fragment);
        transaction.addToBackStack(null);
        // Commit a la transacción
        transaction.commit();
    }

}
