package com.example.myapplication.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.Group;

import java.util.List;

@Dao
public interface GroupDAO {

    @Query("SELECT * FROM groups")
    public List<Group> getGroups();

    /**
     * @Query("SELECT * FROM groups WHERE id = :id")
     * public Group getGroupById(Long id);
     */

    @Query("SELECT * FROM groups WHERE name = :name")
    public Group getGroupByName(String name);

    @Insert
    public void insert(Group... groups);

    @Update
    public void update(Group... groups);

    @Delete
    public void delete(Group group);

    @Query("DELETE FROM groups")
    public void nukeTable();

}
