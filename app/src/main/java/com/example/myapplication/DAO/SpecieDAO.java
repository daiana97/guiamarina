package com.example.myapplication.DAO;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.Specie;

import java.util.List;

@Dao
public interface SpecieDAO {
    @Query("SELECT * FROM species")
    public List<Specie> getSpecies();

    /**
     * @Query("SELECT * FROM species WHERE id = :id")
     * public Specie getSpecieById(int id);
     */

    @Query("SELECT * FROM species WHERE name = :name")
    public Specie getSpecieByName(String name);


    @Insert
    public void insert(Specie... species);

    @Update
    public void update(Specie... species);

    @Delete
    public void delete(Specie specie);


    @Query("SELECT * FROM species WHERE group_id = :groupId")
    public List<Specie> getSpecieByGroup(String groupId);


    @Query("DELETE FROM species")
    public void nukeTable();

}
