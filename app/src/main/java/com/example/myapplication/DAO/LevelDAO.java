package com.example.myapplication.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.Level;

import java.util.List;

@Dao
public interface LevelDAO {

    @Query("SELECT * FROM levels")
    public List<Level> getLevels();

    @Query("SELECT * FROM levels WHERE name = :name")
    public Level getLevelByName(String name);

    @Insert
    public void insert(Level... levels);

    @Update
    public void update(Level... levels);

    @Delete
    public void delete(Level level);

    @Query("DELETE FROM levels")
    public void nukeTable();
}
