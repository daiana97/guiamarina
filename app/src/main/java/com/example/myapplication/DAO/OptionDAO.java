package com.example.myapplication.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.Option;

import java.util.List;

@Dao
public interface OptionDAO {
    @Query("SELECT * FROM options")
    public List<Option> getOptions();

    @Query("SELECT * FROM options WHERE option = :option")
    public Option getOptionByOption(String option);

    @Insert
    public void insert(Option... options);

    @Update
    public void update(Option... options);

    @Delete
    public void delete(Option option);

    @Query("SELECT * FROM options WHERE triviaId = :triviaId")
    public List<Option> getOptionsByTrivia(String triviaId);

    @Query("DELETE FROM options")
    public void nukeTable();
}
