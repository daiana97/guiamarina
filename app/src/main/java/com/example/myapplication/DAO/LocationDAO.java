package com.example.myapplication.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.MyLocation;

import java.util.List;

@Dao
public interface LocationDAO {

    @Query("SELECT * FROM locations")
    public List<MyLocation> getLocations();

    @Query("SELECT * FROM locations WHERE id = :id")
    public MyLocation getLocationsById(Long id);

    @Insert
    public void insert(MyLocation... myLocations);

    @Update
    public void update(MyLocation... myLocations);

    @Delete
    public void delete(MyLocation myLocation);

    @Query("SELECT * FROM locations WHERE nameFile = :nameFile")
    public MyLocation getLocationsByNameFile(String nameFile);

}
