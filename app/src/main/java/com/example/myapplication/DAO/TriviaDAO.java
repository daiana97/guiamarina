package com.example.myapplication.DAO;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.Trivia;

import java.util.List;

@Dao
public interface TriviaDAO {

    @Query("SELECT * FROM trivias")
    public List<Trivia> getTrivias();

    @Query("SELECT * FROM trivias WHERE name = :name")
    public Trivia getTriviaByName(String name);


    @Insert
    public void insert(Trivia... trivias);

    @Update
    public void update(Trivia... trivias);

    @Delete
    public void delete(Trivia trivia);

    @Query("SELECT * FROM trivias WHERE level_id = :levelId")
    public List<Trivia> getTriviaByLevel(String levelId);

    @Query("DELETE FROM trivias")
    public void nukeTable();
}
