package com.example.myapplication.DAO;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplication.models.Challenge;

import java.util.List;

@Dao
public interface ChallengeDAO {

    @Query("SELECT * FROM challenges")
    public List<Challenge> getChallenges();

    @Query("SELECT * FROM challenges WHERE name = :name")
    public Challenge getChallengeByName(String name);

    @Insert
    public void insert(Challenge... challenges);

    @Update
    public void update(Challenge... challenges);

    @Delete
    public void delete(Challenge specie);

    @Query("SELECT * FROM challenges WHERE level_id = :level_Id")
    public List<Challenge> getChallengesByLevel(String level_Id);

    @Query("DELETE FROM challenges")
    public void nukeTable();

}
