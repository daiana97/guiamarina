package com.example.myapplication.models;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "challenges", indices = {@Index(value = {"name"},
        unique = true)})

public class Challenge {


    /**
     * @PrimaryKey(autoGenerate = true)
     * private int id;
     */


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    private String description;
    private int point;
    private int picCount;
    private String level_id;

    @Ignore
    private Category category;


    public Challenge() {
    }


    /**
     * public int getId() {
     * return id;
     * }
     * <p>
     * public void setId(int id) {
     * this.id = id;
     * }
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getPicCount() {
        return picCount;
    }

    public void setPicCount(int picCount) {
        this.picCount = picCount;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String levelId) {
        this.level_id = levelId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }


    @Override
    public String toString() {
        return "Challenge{" +

                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", point=" + point +
                ", picCount=" + picCount +
                ", level=" + level_id +
                ", category=" + category +
                '}';
    }


} // FIN DE LA CLASE
