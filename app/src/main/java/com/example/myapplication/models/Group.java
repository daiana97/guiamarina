package com.example.myapplication.models;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "groups", indices = {@Index(value = {"name"},
        unique = true)})

public class Group implements Serializable {

    // @PrimaryKey(autoGenerate = true)
    // private int id;

    @PrimaryKey
    @ColumnInfo(name = "name")
    @NonNull
    private String name;
    private String description;

    public Group() {
    }

    /**
     * public int getId() {
     * return id;
     * }
     * <p>
     * public void setId(int id) {
     * this.id = id;
     * }
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Group{" +
                " name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }

}
