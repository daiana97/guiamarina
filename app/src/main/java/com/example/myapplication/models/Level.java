package com.example.myapplication.models;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(tableName = "levels", indices = {@Index(value = {"name"},
        unique = true)})
public class Level {

    /**
     * @PrimaryKey(autoGenerate = true)
     * private int id;
     */

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    private int number;
    private int scoreRequired;


    public Level() {
    }


    /**
     * public int getId() {
     * return id;
     * }
     * <p>
     * public void setId(int id) {
     * this.id = id;
     * }
     */


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getScoreRequired() {
        return scoreRequired;
    }

    public void setScoreRequired(int scoreRequired) {
        this.scoreRequired = scoreRequired;
    }

    @Override
    public String toString() {
        return "Level{" +

                ", name='" + name + '\'' +
                ", number=" + number + '\'' +
                ", score Required=" + scoreRequired +
                '}';
    }

}
