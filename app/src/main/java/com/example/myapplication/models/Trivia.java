package com.example.myapplication.models;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.List;


@Entity(tableName = "trivias", indices = {@Index(value = {"name"},
        unique = true)})

public class Trivia {


    /**
     * @PrimaryKey(autoGenerate = true)
     * private int id;
     */

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "name")
    private String name;
    private String description;
    private String question;
    private String optCorrect;
    private int point;
    private String level_id;
    @Ignore
    private Category category;

    @Ignore
    private List<Option> options;


    public Trivia() {
    }


    /**
     * public int getId() {
     * return id;
     * }
     * <p>
     * public void setId(int id) {
     * this.id = id;
     * }
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getOptCorrect() {
        return optCorrect;
    }

    public void setOptCorrect(String optCorrect) {
        this.optCorrect = optCorrect;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public String getLevel_id() {
        return level_id;
    }

    public void setLevel_id(String levelId) {
        this.level_id = levelId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Option> getOptions() {
        return options;
    }

    public void setOptions(List<Option> options) {
        this.options = options;
    }


    @Override
    public String toString() {
        return "Trivia{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", question='" + question + '\'' +
                ", optCorrect='" + optCorrect + '\'' +
                ", point=" + point +
                ", levelId='" + level_id + '\'' +
                ", category=" + category +
                ", options=" + options +
                '}';
    }
} // FIN CLASE

