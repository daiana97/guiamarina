package com.example.myapplication.models;

public class ImageChUser {

    private String chUser_id;
    private String path;
    private Double latitud;
    private Double longitud;

    public ImageChUser() {
    }

    public String getChUser_id() {
        return chUser_id;
    }

    public void setChUser_id(String chUser_id) {
        this.chUser_id = chUser_id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }
}
