package com.example.myapplication.models;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "options")
public class Option {


    @PrimaryKey
    @NonNull
    private String option;

    private String triviaId;


    public String getTriviaId() {
        return triviaId;
    }

    public void setTriviaId(String triviaId) {
        this.triviaId = triviaId;
    }


    public Option() {
    }


    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }


    @Override
    public String toString() {
        return "Option{" +
                ", option='" + option + '\'' +

                '}';
    }

} // FIN DE LA CLASE
