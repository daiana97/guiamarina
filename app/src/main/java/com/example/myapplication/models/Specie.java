package com.example.myapplication.models;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.io.Serializable;


@Entity(tableName = "species", indices = {@Index(value = {"name"},
        unique = true)})

public class Specie implements Serializable {


    /**
     * @PrimaryKey(autoGenerate = true)
     * private int id;
     */

    @PrimaryKey
    @ColumnInfo(name = "name")
    @NonNull
    private String name;
    private String scientificName;
    private String description;
    private String diet;
    private String habitat;
    private String reproduction;
    private String group_id;


    // private byte[] image;
    private String image;


    public Specie() {
    }

    /**
     * public int getId() {
     * return id;
     * }
     * <p>
     * public void setId(int id)
     */

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiet() {
        return diet;
    }

    public void setDiet(String diet) {
        this.diet = diet;
    }

    public String getHabitat() {
        return habitat;
    }

    public void setHabitat(String habitat) {
        this.habitat = habitat;
    }

    public String getReproduction() {
        return reproduction;
    }

    public void setReproduction(String reproduction) {
        this.reproduction = reproduction;
    }

    /**
     * public byte[] getImage() {
     * return image;
     * }
     * <p>
     * public void setImage(byte[] image) {
     * this.image = image;
     * }
     */

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    @Override
    public String toString() {
        return "Specie{" +
                ", name='" + name + '\'' +
                ", scientificName='" + scientificName + '\'' +
                ", description='" + description + '\'' +
                ", diet='" + diet + '\'' +
                ", habitat='" + habitat + '\'' +
                ", reproduction='" + reproduction + '\'' +
                ", group=" + group_id +
                '}';
    }


}
