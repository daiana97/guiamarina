package com.example.myapplication.models;

import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "images")
public class Image {
    @PrimaryKey(autoGenerate = true)
    private Long id;
    private String image;
    private String challengeId;
    private boolean send;
    private String init_challenge;
    private String finish_challenge;

    public Image() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public boolean isSend() {
        return send;
    }

    public void setSend(boolean send) {
        this.send = send;
    }

    public String getInit_challenge() {
        return init_challenge;
    }

    public void setInit_challenge(String init_challenge) {
        this.init_challenge = init_challenge;
    }

    public String getFinish_challenge() {
        return finish_challenge;
    }

    public void setFinish_challenge(String finish_challenge) {
        this.finish_challenge = finish_challenge;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", challengeId=" + challengeId +
                ", send=" + send +
                '}';
    }
}
