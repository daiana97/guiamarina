package com.example.myapplication.Utils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.room.Room;

import com.example.myapplication.DAO.ImageDAO;
import com.example.myapplication.DAO.LocationDAO;
import com.example.myapplication.models.ChallengeUser;
import com.example.myapplication.models.Image;
import com.example.myapplication.models.ImageChUser;
import com.example.myapplication.models.MyLocation;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.util.List;

public class FirebaseUtil {

    private DatabaseReference mDatabase;
    private ImageDAO imageDAO;
    private LocationDAO locationDAO;
    private MyLocation location;
    private Context context;
    private StorageReference storageRef;

    public FirebaseUtil(Context context) {
        mDatabase = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReference();
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        DataBase database = Room.databaseBuilder(context, DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        imageDAO = database.getImageDAO();
        locationDAO = database.getLocationDAO();
        location = new MyLocation();
    }

    public void writeNewChallengeUser() {
        //Crear el desafio del usuario para guardarlo en la base de datos
        List<Image> listImages = imageDAO.getImageBySend(false);
        File imageFile;

        ChallengeUser cUser = null;
        String idChUser = String.valueOf(mDatabase.push().getKey());
        for (Image item : listImages) {

            String pathOriginal = item.getImage();
            String challengeId = String.valueOf(item.getChallengeId());
            String path = pathOriginal.substring(7);
            path = path.replace("%20", " ");

            location = locationDAO.getLocationsByNameFile(path);

            imageFile = new File(path);
            String fileName = imageFile.getName();
            Log.d("nameFile ", fileName);

            if (cUser == null) {
                cUser = new ChallengeUser();
                cUser.setChallenge_id(challengeId);
                cUser.setInit_challenge(item.getInit_challenge());
                cUser.setFinish_challenge(item.getFinish_challenge());
                sendChallengefinish(cUser, item, imageFile, fileName, idChUser);
            } else if (!cUser.getChallenge_id().equals(challengeId) || !cUser.getInit_challenge().equals(item.getInit_challenge()) || !cUser.getFinish_challenge().equals(item.getFinish_challenge())) {
                idChUser = String.valueOf(mDatabase.push().getKey());
                cUser.setChallenge_id(challengeId);
                cUser.setInit_challenge(item.getInit_challenge());
                cUser.setFinish_challenge(item.getFinish_challenge());
                sendChallengefinish(cUser, item, imageFile, fileName, idChUser);
            } else {
                sendImages(item, imageFile, fileName, idChUser);
            }

        }

    }

    private void sendChallengefinish(ChallengeUser cUser, Image item, File imageFile, String fileName, String idChUser) {

        String userId = SharedPrefUtils.get(this.context, "id");
        String userName = SharedPrefUtils.get(this.context, "username");


        cUser.setUser_id(userId);
        cUser.setUser_name(userName);
        mDatabase.child("ChallengesUsers").child(idChUser).setValue(cUser);
        sendImages(item, imageFile, fileName, idChUser);


    }

    private void sendImages(Image item, File imageFile, String fileName, String idChUser) {

        ImageChUser imageChUser = new ImageChUser();
        imageChUser.setChUser_id(idChUser);
        imageChUser.setPath(fileName);
        imageChUser.setLatitud(location.getLatitud());
        imageChUser.setLongitud(location.getLongitud());

        byte[] bytesArray = new byte[(int) imageFile.length()];

        mDatabase.child("ChallengesUsers").child(idChUser).child("ImagesChallengesUsers").push().setValue(imageChUser);
        storageRef.child("ImagesChallengesUser").child(fileName).putFile(Uri.fromFile(imageFile));

        this.updateImage(item);
    }

    private void updateImage(Image data) {
        data.setSend(true);
        imageDAO.update(data);
    }


}
