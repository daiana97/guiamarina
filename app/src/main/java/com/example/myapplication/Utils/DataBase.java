package com.example.myapplication.Utils;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.myapplication.DAO.ChallengeDAO;
import com.example.myapplication.DAO.GroupDAO;
import com.example.myapplication.DAO.ImageDAO;
import com.example.myapplication.DAO.LevelDAO;
import com.example.myapplication.DAO.LocationDAO;
import com.example.myapplication.DAO.OptionDAO;
import com.example.myapplication.DAO.SpecieDAO;
import com.example.myapplication.DAO.TriviaDAO;
import com.example.myapplication.models.Challenge;
import com.example.myapplication.models.Group;
import com.example.myapplication.models.Image;
import com.example.myapplication.models.Level;
import com.example.myapplication.models.MyLocation;
import com.example.myapplication.models.Option;
import com.example.myapplication.models.Specie;
import com.example.myapplication.models.Trivia;

@Database(entities = {Image.class, Group.class, MyLocation.class, Specie.class, Level.class, Challenge.class, Trivia.class, Option.class}, version = 1, exportSchema = false)
public abstract class DataBase extends RoomDatabase {

    public abstract ImageDAO getImageDAO();

    public abstract GroupDAO getGroupDAO();

    public abstract LocationDAO getLocationDAO();

    public abstract SpecieDAO getSpecieDAO();

    public abstract LevelDAO getLevelDAO();


    public abstract ChallengeDAO getChallengeDAO();

    public abstract TriviaDAO getTriviaDAO();

    public abstract OptionDAO getOptionDAO();

}
