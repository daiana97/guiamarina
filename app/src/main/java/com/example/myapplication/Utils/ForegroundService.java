package com.example.myapplication.Utils;


import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.room.Room;

import com.example.myapplication.DAO.ImageDAO;
import com.example.myapplication.DAO.LocationDAO;
import com.example.myapplication.R;
import com.example.myapplication.models.Image;
import com.example.myapplication.models.MyLocation;

import java.io.File;
import java.util.List;

import static com.example.myapplication.App.CHANNEL_ID;

public class ForegroundService extends Service {
    private ImageDAO imageDAO;
    // private OkHttpClient client;
    private LocationDAO locationDAO;
    private MyLocation location;
    private int IdcUSer;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        init();
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(String.valueOf(R.string.noti_fore))
                .setContentText("Enviando desafios")
                .setAutoCancel(true)
                .build();

        startForeground(1, notification);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                sentChallenges();
            }
        });

        thread.start();

        return START_NOT_STICKY;
    }

    private void init() {
        DataBase database = Room.databaseBuilder(this, DataBase.class, "proyectoSub")
                .allowMainThreadQueries()
                .build();
        imageDAO = database.getImageDAO();
        locationDAO = database.getLocationDAO();
        // client = new OkHttpClient();
        location = new MyLocation();
    }

    private void sentChallenges() {
        List<Image> listImages = imageDAO.getImageBySend(false);
        Log.d("FS-sentchallenges", listImages.toString());

        File imageFile;

/*
        ChallengeUser cUser = null;
        for (Image item : listImages) {

            String pathOriginal = item.getImage();
            String challengeId = String.valueOf(item.getChallengeId());
            String path = pathOriginal.substring(7, pathOriginal.length());
            path = path.replace("%20", " ");

            Log.d("FS-sentChallenges", path);
            location = locationDAO.getLocationsByNameFile(path);

            imageFile = new File(path);
            String filePath = imageFile.getAbsolutePath();

            //Bitmap bitmap = ImageUtils.getInstant().getCompressedBitmap(filePath);

            if (cUser == null) {
                cUser = new ChallengeUser();
                cUser.setChallenge_id(challengeId);
                cUser.setInit_challenge(item.getInit_challenge());
                cUser.setFinish_challenge(item.getFinish_challenge());
                sendChallengefinish(cUser, item, imageFile, filePath);
            } else if (!cUser.getChallenge_id().equals(challengeId) || !cUser.getInit_challenge().equals(item.getInit_challenge()) || !cUser.getFinish_challenge().equals(item.getFinish_challenge())) {
                cUser.setChallenge_id(challengeId);
                cUser.setInit_challenge(item.getInit_challenge());
                cUser.setFinish_challenge(item.getFinish_challenge());
                sendChallengefinish(cUser, item, imageFile, filePath);
            } else {
                sendImages(item, imageFile, filePath);
            }

        }*/
        stopSelf();
    }

    private void sendImages(Image image, File imageFile, String filePath) {

/*
        MediaType mediaType = MediaType.parse("multipart/form-data");
        RequestBody requestBody = null;

        requestBody = new MultipartBody.Builder()
                .setType(mediaType)
                .addFormDataPart("image", filePath.substring(filePath.lastIndexOf("/") + 1), RequestBody.create(
                        MediaType.parse("image/*"), imageFile))
                .addFormDataPart("latitud", String.valueOf(location.getLatitud()))
                .addFormDataPart("longitud", String.valueOf(location.getLongitud()))
                .addFormDataPart("challenge_user_id", String.valueOf(IdcUSer)).build();

        Request request1 = new Request.Builder()
                .url(Connection.URL_WEB_SERVICES + "sendImages")
                .post(requestBody)
                .addHeader("content-Type", "multipart/form-data")
                .build();

        try (Response response1 = client.newCall(request1).execute()) {
            if (!response1.isSuccessful()) {
                throw new IOException("Error: " + response1);
            }

            String body1 = response1.body().string();
            JSONObject jsonObject1 = new JSONObject(body1);
            Log.d("ENVIAR IMAGEN", String.valueOf(jsonObject1.getJSONObject("data")));
            this.updateImage(image);
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }*/
    }

   /* private void sendChallengefinish(ChallengeUser cUser, Image image, File imageFile, String filePath) {

        String userId = SharedPrefUtils.get(getApplicationContext(), "id");
        MediaType mediaType = MediaType.parse("multipart/form-data");
        RequestBody requestBody, requestBodyUser = null;

        Log.d("USERID ", userId);
        //armar request para datos del desafio del usuario
        if (userId == null) {
            requestBodyUser = new MultipartBody.Builder()
                    .setType(mediaType)
                    .addFormDataPart("challenge_id", cUser.getChallenge_id())
                    .addFormDataPart("init_challenge", cUser.getInit_challenge())
                    .addFormDataPart("finish_challenge", cUser.getFinish_challenge())
                    .build();
        } else {
            requestBodyUser = new MultipartBody.Builder()
                    .setType(mediaType)
                    .addFormDataPart("challenge_id", cUser.getChallenge_id())
                    .addFormDataPart("init_challenge", cUser.getInit_challenge())
                    .addFormDataPart("finish_challenge", cUser.getFinish_challenge())
                    .addFormDataPart("user_id", userId)
                    .build();
        }

        Request request = new Request.Builder()
                .url(Connection.URL_WEB_SERVICES + "sendChallenge")
                .post(requestBodyUser)
                .addHeader("content-Type", "multipart/form-data")
                .build();


        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                throw new IOException("Error: " + response);
            }

            String body = response.body().string();
            JSONObject jsonObject = new JSONObject(body);
            IdcUSer = jsonObject.getJSONObject("data").getInt("id");

            requestBody = new MultipartBody.Builder()
                    .setType(mediaType)
                    .addFormDataPart("image", filePath.substring(filePath.lastIndexOf("/") + 1), RequestBody.create(
                            MediaType.parse("image/*"), imageFile))
                    .addFormDataPart("latitud", String.valueOf(location.getLatitud()))
                    .addFormDataPart("longitud", String.valueOf(location.getLongitud()))
                    .addFormDataPart("challenge_user_id", String.valueOf(IdcUSer)).build();

            Request request1 = new Request.Builder()
                    .url(Connection.URL_WEB_SERVICES + "sendImages")
                    .post(requestBody)
                    .addHeader("content-Type", "multipart/form-data")
                    .build();

            try (Response response1 = client.newCall(request1).execute()) {
                if (!response1.isSuccessful()) {
                    throw new IOException("Error: " + response1);
                }

                String body1 = response1.body().string();
                JSONObject jsonObject1 = new JSONObject(body1);
                Log.d("ENVIAR IMAGEN", String.valueOf(jsonObject1.getJSONObject("data")));
                this.updateImage(image);
            }
            //Aca tengo que actualizar la imagen del desafio.
            //this.updateImage(image);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onDestroy() {
        Log.d("ONDESTROY", "PINTO LA DESTRUCION");
        super.onDestroy();
    }

    private void updateImage(Image data) {
        data.setSend(true);
        imageDAO.update(data);
    }

}
