package com.example.myapplication.Utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.myapplication.App;

public class ConnectionChangeReciver extends BroadcastReceiver {
    private ControllerConnection ctrlConnection;

    @Override
    public void onReceive(Context context, Intent intent) {
        ctrlConnection = new ControllerConnection(context);
        boolean goodConex = ctrlConnection.comprobarBuenaConexion();
        App.getInstance().setGoodConecction(goodConex);
    }

}
